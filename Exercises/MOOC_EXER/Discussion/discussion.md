# Discussions

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Chapter01: Overview](#chapter01-overview)
2. [Chapter02: Process Management](#chapter02-process-management)
    1. [分析实现互斥的各种方法的要点](#分析实现互斥的各种方法的要点)
3. [Chapter03: Memory Management](#chapter03-memory-management)
    1. [逻辑地址和物理地址之间有什么区别？](#逻辑地址和物理地址之间有什么区别)
    2. [分页和分段之间有什么区别？](#分页和分段之间有什么区别)
    3. [简单分页和虚拟分页有何区别？](#简单分页和虚拟分页有何区别)
    4. [如何实现段页式虚拟存储？](#如何实现段页式虚拟存储)
4. [Chapter04: Device Management](#chapter04-device-management)
5. [Chapter05: File Management](#chapter05-file-management)

<!-- /code_chunk_output -->

## Chapter01: Overview

## Chapter02: Process Management

### 分析实现互斥的各种方法的要点

## Chapter03: Memory Management

### 逻辑地址和物理地址之间有什么区别？

### 分页和分段之间有什么区别？

### 简单分页和虚拟分页有何区别？

### 如何实现段页式虚拟存储？

1. 地址空间：
    1. 逻辑地址： 段号 + 段内页号 + 页内偏移
    1. 物理地址： 页框号 + 页内偏移
1. 硬件： `MMU`, `段表寄存器`，`swap area`
1. OS： 段表、页表

## Chapter04: Device Management

## Chapter05: File Management
