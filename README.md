# 关于《操作系统原理》课程

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于本仓库](#关于本仓库)
    1. [本仓库许可协议](#本仓库许可协议)
    2. [本仓库编写环境和使用方法](#本仓库编写环境和使用方法)
    3. [帮助完善本仓库](#帮助完善本仓库)
    4. [本仓库组织](#本仓库组织)
2. [关于本课程](#关于本课程)
    1. [课程基本信息](#课程基本信息)
    2. [课程教材与实验教材](#课程教材与实验教材)
3. [参考资料](#参考资料)
    1. [专著](#专著)
    2. [大学课程](#大学课程)
    3. [其他](#其他)

<!-- /code_chunk_output -->

## 关于本仓库

### 本仓库许可协议

本仓库使用`CC-BY-SA-4.0`协议，更详细的协议内容请见<https://gitlab.com/arm_commons/commons/-/blob/master/LICENSES/CC-BY-SA-4.0/README.md>

### 本仓库编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>

### 帮助完善本仓库

期待您一起完善本仓库，您可以通过以下方式帮助完善本仓库：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`或`Gitee`的`issue`发起一个新的`issue`（标签设置成`optimize`）

本仓库地址：

1. 主地址： <https://gitlab.com/arm_courses/os>
1. 镜像地址： <https://gitee.com/aroming/course_os>

### 本仓库组织

1. 目录组织
    1. **`Addons/`** ： 附加文档，如：专题类的论述
    1. **`Experiments/`** ： 实验指导文档
    1. **`Outlines/`:** ： 各部分的大纲
    1. **`Exercises`** ： 习题及其解析
    1. **`README.md`** ： 本文件
1. 章节对应
    1. **`Part_01`** 或 **`pt01`** ： Operating System Overview操作系统概述
    1. **`Part_02`** 或 **`pt02`** ： Process Management进程管理
    1. **`Part_03`** 或 **`pt03`** ： Memory Management内存管理
    1. **`Part_04`** 或 **`pt04`** ： Device Management设备管理
    1. **`Part_05`** 或 **`pt05`** ： File Management文件管理

## 关于本课程

### 课程基本信息

1. 课程名称： 《计算机操作系统/操作系统》（ _Computer Operating System_ ），专业基础课，计算机类硕士研究生入学考试初试专业课统考课程（四门课程之一）
1. 先修课程： 《计算机组成原理》（或包含组成原理内容的相关课程），《微机原理与接口技术》，《接口与通讯》，《C语言程序设计》，《数据结构》
1. 课程目标：
    1. **When && Who && Why** ： 了解OS的源由和演变历史
    1. **What** ： 掌握OS的重要概念和功能
    1. **How** ： 掌握OS的基本原理、经典算法与数据结构
1. Keywords： Technical Terms, Principles, Algorithms, Data Structures

### 课程教材与实验教材

1. :book: 课程教材：[《计算机操作系统（第3版）》](http://www.tup.tsinghua.edu.cn/booksCenter/book_07794401.html)，郁红英等编著，清华大学出版社
1. :book: 实验教材：[《计算机操作系统实验指导（第3版）》](http://www.tup.tsinghua.edu.cn/booksCenter/book_07794301.html)，郁红英（主编），清华大学出版社
1. :book: 辅助教材：[_OSIDP(Operating Systems Internals and Design Principles 9th Global Edition)_](https://www.pearson.com/us/higher-education/program/Stallings-Operating-Systems-Internals-and-Design-Principles-9th-Edition/PGM1262980.html)[^1], [美]William Stallings, Pearson

[^1]: 中文翻译版为《操作系统-精髓与设计原理》

## 参考资料

### 专著

1. :book: [《现代操作系统（原书第4版）》](http://www.cmpbook.com/stackroom.php?id=43456)，[荷-美]Andrew S. Tanenbaum，Herbert Bos 著，陈向群 译， 机械工业出版社，[豆瓣链接](https://book.douban.com/subject/27096665/)
1. :book: [《深入理解计算机系统（第1版.修订版）》](), [美]Randal E.Bryant, David O'Hallaron著，龚奕利， 雷迎春译. 中国电力出版社. ISBN: 7-5083-2175-8. 2004-10，[豆瓣链接](https://book.douban.com/subject/1230413/)，[CS: APP blog](http://csappbook.blogspot.com/)
1. :book: :computer: [《UNIX编程艺术》](https://www.phei.com.cn/module/goods/wssd_content.jsp?bookid=33367)，[美] Eric S.Raymond 著，姜宏，何源，蔡晓骏 译，电子工业出版社，[豆瓣链接](https://book.douban.com/subject/1467587/)，[英文在线阅读官网](http://www.catb.org/~esr/writings/taoup/html/)
1. :book: :computer: [《只是为了好玩 : Linux之父林纳斯自传》](https://github.com/limkokhole/just-for-fun-linus-torvalds)，[芬-美]Linus Torvalds，David Diamond，[豆瓣链接](https://book.douban.com/subject/25930025/)，[GitHub链接（英文EPUB，PDF）](https://github.com/limkokhole/just-for-fun-linus-torvalds)
1. :book: :computer: [《鸟哥的Linux私房菜：基础学习篇（第4版）》](http://linux.vbird.org/linux_basic/)，[豆瓣链接](https://book.douban.com/subject/30359954/)，[GitBook链接](https://legacy.gitbook.com/book/wizardforcel/vbird-linux-basic-4e/details)
1. :book: :computer: [《鸟哥的Linux私房菜：服务器架设篇（第3版）》](http://linux.vbird.org/linux_server/)，[豆瓣链接](https://book.douban.com/subject/10794788/)，[GitBook链接](https://legacy.gitbook.com/book/wizardforcel/vbird-linux-server-3e/details)
1. :book: 《操作系统设计与实现（原书第3版）》（[上册](https://www.phei.com.cn/module/goods/wssd_content.jsp?bookid=43010)，下册），[荷-美] Andrew S. Tanenbaum），[美] Albert S. Woodhull 著，陈渝，谌卫军 译，电子工业出版社，[豆瓣链接 -- 上册](https://book.douban.com/subject/2044818/)，[豆瓣链接 -- 下册](https://book.douban.com/subject/2044819/)

### 大学课程

1. :computer: [中国大学MOOC：电子科技大学《计算机操作系统》](https://www.icourse163.org/learn/UESTC-1205790811?tid=1206950287#/learn/announce)
1. :computer: [清华大学：操作系统2019春](http://os.cs.tsinghua.edu.cn/oscourse/OS2019spring)
    + [课程问答](https://xuyongjiande.gitbooks.io/os-qa/index.html)
    + [实验指导书](https://chyyuu.gitbooks.io/ucore_os_docs/content/)
    + [实验源码](https://github.com/chyyuu/ucore_os_lab)
1. :computer: [中国大学MOOC：华中科技大学《操作系统原理》](https://www.icourse163.org/course/HUST-1003405007#/info)
1. :computer: [Stony Brook University: CSE 306 Operating Systems](https://www3.cs.stonybrook.edu/~kifer/Courses/cse306/)
1. :computer: [University of North Florida: COP 4610 Operating Systems](https://www.unf.edu/public/cop4610/)

### 其他

1. :computer: [Mac OS X 背后的故事](https://bigeast.github.io/Mac_OS_X_%E8%83%8C%E5%90%8E%E7%9A%84%E6%95%85%E4%BA%8B.html)
1. :computer: [易百教程：操作系统](https://www.yiibai.com/os)
