# Part 01: Operating System Overview操作系统概述

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Preface](#preface)
    1. [Main TOC for Learning](#main-toc-for-learning)
    2. [Keys for Programming](#keys-for-programming)
2. [What is Operating System操作系统是什么](#what-is-operating-system操作系统是什么)
3. [OS的需求分析](#os的需求分析)
4. [OS的类型](#os的类型)
    1. [按功能特征分类（基本分类）](#按功能特征分类基本分类)
    2. [按并发度分类](#按并发度分类)
    3. [按应用场景分类](#按应用场景分类)
5. [OS的重要概念](#os的重要概念)
    1. [Job, Process/Task and Thread](#job-processtask-and-thread)
    2. [Concurrency and Parallel](#concurrency-and-parallel)
    3. [Memory and Storage](#memory-and-storage)
    4. [Virtual Memory and File](#virtual-memory-and-file)
    5. [Off-line IO, On-line IO and SPOOLing IO](#off-line-io-on-line-io-and-spooling-io)
6. [OS的架构](#os的架构)
    1. [Modularization Perspective](#modularization-perspective)
    2. [Privileged Perspective](#privileged-perspective)
    3. [Kernel Process Perspective](#kernel-process-perspective)
7. [OS Distribution Issues](#os-distribution-issues)
    1. [OS, OSS and FOSS](#os-oss-and-foss)
    2. [POSIX&reg;, SUS&reg;/UNIX&reg; and UNIX-like](#posixsupregsup-sussupregsupunixsupregsup-and-unix-like)
    3. [Is Linux a POSIX :question:](#is-linux-a-posix-question)
8. [OS Timelines](#os-timelines)
    1. [UNIX-like Timeline](#unix-like-timeline)
        1. [UNIX and UNIX-like Timeline](#unix-and-unix-like-timeline)
        2. [Linux Distribution Timeline](#linux-distribution-timeline)
    2. [Non-UNIX-like Timeline](#non-unix-like-timeline)
        1. [Windows Timeline](#windows-timeline)

<!-- /code_chunk_output -->

## Preface

### Main TOC for Learning

1. what is `OS`: the definition, functions, types, architectures of `OS`
1. `OS` in theory and in real world
1. main `OS` in real world
1. `OS`, `OSS` and `FOSS`
1. `OS` timelines

### Keys for Programming

1. distinguish and choose what `OS` we based on
1. terms what we should know and use in communication

## What is Operating System操作系统是什么

1. an operating system (`OS`) is system software that manages computer hardware, software resources, and provides common services for computer programs.
1. short history of `OS`
    1. 1st generation: no `OS`
    1. 2nd generation: `simple batch system`/`monitor`
    1. 3rd generation: `multiprogramming batch system`, `time-sharing system`
    1. 4st generation:  
1. 什么样的`OS`是`Modern OS`？/`Modern OS`的特征是什么？
    1. concurrency并发（最重要、最基本的特征）
    1. sharing共享（两个基本特征之一）
    1. virtual虚拟
    1. indeterminacy/asynchronous不确定性/异步
1. `Traditional OS`发展成`Modern OS`的原因和动力是什么？
    1. 硬件的发展
        1. 指令集的发展
        1. 性能的发展
        1. 通信的发展： 机内通信、外设通信、网络通信
        1. 内存的发展
        1. 外存的发展
    1. 应用的需要：应用的需要同时会促进硬件和`OS`的发展
        1. 更有效地使用
        1. 更多的新应用
    1. 安全的需要：
        1. 4A(Authentication, Authorization, Account, Audit)
1. `OS`是什么？4种观点还是2种观点？ = = > 2种观点似乎更合理
    1. 资源管理的观点： 软硬件的资源管理器，负责分配、监控、回收软硬件资源
    1. 用户的观点/扩展机器的观点/虚拟机的观点： 在祼机的物质基础上叠加一层软件，从而使原来的祼机更强大、更易用
1. `OS`对硬件的依赖
    1. `timer`定时器
    1. `IO interrupt`中断
    1. `DMA` or `channel`
    1. `privileged instructions`特权指令
    1. `MPM(Memory Protection Mechanism)`/`MMU(Memory Management Unit)`内存管理单元 = = > micro-processor from `Intel 80386`

## OS的需求分析

1. `OS`的目标：
    1. 提高资源的效率
    1. 方便用户的使用
1. `OS`的核心功能需求：
    1. process management（进程管理）/`processor management`（处理机管理）
    1. memory management（内存管理）
    1. device management（设备管理）
    1. file management（文件管理）
    1. interface（接口）：
        1. end-user interface（最终用户接口）
            1. online end-user command interface（联机用户命令接口）
                + `CLI`(Command Line Interface)
                + `TWIN`(Textmode WINdow): eg, `top`, `nano`, `vim`
            1. offline end-user command interface（脱机用户命令接口）
                + `JCL`(Job Control Language) /`batch processing`
        1. program interface/system call（程序接口/系统调用）:
            + `POSIX`(Portable Operating System Interface)/`LSB`(Linux Standards Base)
            + `WinAPI`: `Win16`, `Win32`, `Win32s`, `Win64`, `WinCE`
1. `OS`的非功能需求：
    1. 外部质量属性：
        1. performance/efficiency（性能/效率）：
            1. throughput（吞吐量）
            1. response time（响应时间）
            1. processing time（处理时间）
            1. turnaround time（周转时间）
            1. processor utilization（处理机利用率）
        1. fairness（公平性）
        1. reliability（可靠性）
        1. security（安全性）
    1. 内部质量属性：
        1. scalability（可伸缩性）
        1. extensibility（可扩展性）
        1. portability（可移植性）

## OS的类型

### 按功能特征分类（基本分类）

1. **batch processing OS** 批处理操作系统： 强调系统的总吞吐量
1. **time sharing OS** 分时操作系统： 强调响应时间
1. **real time OS** 实时操作系统： 强调处理截止时间

### 按并发度分类

1. **single user, single tasking `OS`** 单用户单任务操作系统 ： `DOS`
1. **single user, multi tasking `OS`** 单用户多任务操作系统 ： `Windows`
1. **multi user, multi tasking `OS`** 多用户多任务操作系统 ： `UNIX-like`

### 按应用场景分类

1. **microcomputer OS**: `macOS`, `GUN/Linux`, `Windows`, `DOS`
1. **multi-processor or multi-core OS**: `UNIX-like`, `Windows`(`Windows Vista` and later)
    + `SMP`(Symmetric MultiProcessing, 对称多处理器): processors are connected to a single, shared main memory, have full access to all input and output devices, and are controlled by a single operating system instance that treats all processors equally(defined by wiki)
1. **network OS**: `UNIX-like`, `Windows`
1. **distributed OS**:  `Hadoop`
1. **embedded OS**:  `iOS`, `iPadOS`, `watchOS`, `tvOS`,`Android`

## OS的重要概念

### Job, Process/Task and Thread

1. `job`: user perspective/application perspective, one `job` contain one or more `process`
1. `process` and `thread`: `OS` perspective
    + `process`: resource allocate unit
    + `thread`: schedule unit(`kernel thread`)

### Concurrency and Parallel

1. **`concurrency`** : processing one process in micro-time, but as do some jobs at the same time in macro-view
1. **`parallel`** : processing more than process in the same time, require more than one processors

### Memory and Storage

1. **`memory`** / **`main memory`** / **`primary memory`** 内存/主存
1. **`storage`** / **`secondary storage`** / **`secondary memory`** 外存/辅存

### Virtual Memory and File

1. **`virtual memory`** : each process action as owns the whole memory address, need translate from `logical address` to `physical address`
1. **`file`** : abstract of IO device(in `UNIX-like`)

### Off-line IO, On-line IO and SPOOLing IO

1. `off-line IO`
1. `on-line IO`
1. `SPOOLing IO`: Simultaneous Peripheral Operations On-Line（外部设备联机并行操作/假脱机技术）
    + **purpose:** usually used for mediating between computer and slow peripheral, such as a printer
    + **key technology:**
        1. virtual: one peripheral virtual to many logical peripheral
        1. queue

## OS的架构

### Modularization Perspective

1. layered structural style（分层结构风格）
1. hierarchical structural style（分级结构风格）
1. modular structural style（分块结构风格）

### Privileged Perspective

1. Multi-Mode Structural Style（多模式结构风格）
1. Dual-Mode Structural Style（双模式结构风格）: `user mode` and `kernel mode`
1. Single-Mode Structural Style（单模式结构风格）

### Kernel Process Perspective

1. micro kernel（微内核）: small `OS` core that contains only essential `OS` functions, eg: `macOS`, `Windows NT`
    1. **client-server architecture**
    1. **multi processes, multi address**: the kernel is broken down into separate processes, known as servers. Some of the servers run in kernel space and some run in user-space, all servers are kept separate and run in different address spaces
    1. **IPC communication**: servers invoke "services" from each other by sending messages via IPC (Inter Process Communication)
1. monolithic kernel（单体内核）: most of `UNIX`, `Linux`
    1. **single process, single address**: a single large process running entirely in a single address space, all kernel services exist and execute in the kernel address space, and a single static binary file
    1. **invoke directly**: the kernel can invoke functions directly.

>**see furthermore**
>
><https://stackoverflow.com/a/5194239>

## OS Distribution Issues

### OS, OSS and FOSS

+ **`OS`** : Operating System
+ **`OSS`** : Open-Source Software
+ **`FOSS`** : Free, Open-Source Software

### POSIX<sup>&reg;</sup>, SUS<sup>&reg;</sup>/UNIX<sup>&reg;</sup> and UNIX-like

1. **`POSIX`(Portable Operating Systems Interface) certification**
    + defines rules for a standard behavior and interface, such as:
        + standard C operations
        + multi-tasking
        + error states
        + command line && commands
    + standard enforced by IEEE && maintained by Austin Group (IEEE Computer Society, The Open Group, ISO/IEC JTC 1)
        + <http://get.posixcertified.ieee.org/certification_guide.html>
        + implements a set of automated conformance tests
        + need fee to certified( **at least \$2,000/annum** ), <http://get.posixcertified.ieee.org/docs/posix-fee-schedule-1.3.PDF>
1. **`SUS`(Single UNIX Specification)/`UNIX` certification**
    + `UNIX` is a trademark and brand, being `UNIX` certified(testing for compliance with `SUS`) makes the `OS` being a `UNIX`
    + developed and maintained by the Austin Group
    + significant licence fee( **\$1,000/annum + many many others** ), <https://www.opengroup.org/openbrand/Brandfees.htm>
    + UNIX<sup>&reg;</sup> Certified Products, <https://www.opengroup.org/openbrand/register/> and <https://www.opengroup.org/openbrand/register/index2.html>
1. `UNIX-like`: behaves in a manner similar to a UNIX system, sometimes referred to as UN*X or *nix

>Back in the day, there were many differences between `SUS` and `POSIX`, but today `SUS` is just `POSIX + Curses`.(`Curses` is terminal control library for `UNIX-like` systems)
>see futhermore: <https://mywiki.wooledge.org/POSIX>

### Is Linux a POSIX :question:

`Linux` is not `POSIX` and not `SUS` , for these reasons:

1. `Linux` is a kernel, and `POSIX` does not specify a kernel interface
1. (:warning: **unconfirmed** )`POSIX` certification need fee, so `Linux` is just **mostly `POSIX-compliant`**
1. (:warning: **unconfirmed** )`SUS` certification need more fee, except below `Linux` distros:
    + [Huawei EulerOS](https://developer.huaweicloud.com/ict/cn/site-euleros/euleros)
    + [Inspur K-UX](https://en.inspur.com/en/2402170/2402191/2407691/index.html), **certification has expired**

foremost, `Linux` is based on `LSB` which is based on `POSIX`,`SUS`, and several other open standards, but extends them in certain areas.

>**see furthermore**
>
>1. [sirredbeard/Awesome-UNIX](https://github.com/sirredbeard/Awesome-UNIX#unix-certified-linux-based-operating-systems)
>1. [Linux Standards](https://www.linux.org/threads/linux-standards.11759/)
>1. [Posix Standard](https://linuxhint.com/posix-standard/)

## OS Timelines

>**see furthermore**
>
>1. <https://docs.docker.com/install/>
>1. <https://www.levenez.com/>
>1. UNIX发展史(BSD,GNU,linux)： <https://blog.51cto.com/yjplxq/1034705>

### UNIX-like Timeline

#### UNIX and UNIX-like Timeline

![unix_generations](./assets_image/unix_generations.jpg)

#### Linux Distribution Timeline

![gldt1202](./assets_image/gldt1202.svg)

### Non-UNIX-like Timeline

<http://shannon.usu.edu.ru/History/oshistory/>

#### Windows Timeline

<https://www.levenez.com/windows/>
