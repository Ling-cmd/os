# Part 02: Process Management进程管理

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=4 orderedList=true} -->

<!-- code_chunk_output -->

1. [Preface](#preface)
    1. [Main TOC for Learning](#main-toc-for-learning)
    2. [Keys for Programming](#keys-for-programming)
2. [Process and Thread进程与线程](#process-and-thread进程与线程)
    1. [What is Process进程是什么](#what-is-process进程是什么)
    2. [Process Description进程描述](#process-description进程描述)
    3. [What is Tread线程是什么](#what-is-tread线程是什么)
    4. [Thread Description线程描述](#thread-description线程描述)
3. [Process Control进程控制](#process-control进程控制)
    1. [What is Process Control进程控制是什么](#what-is-process-control进程控制是什么)
    2. [Process Control and States Transfer进程控制与进程状态转换](#process-control-and-states-transfer进程控制与进程状态转换)
    3. [Process Control Primitives进程控制原语](#process-control-primitives进程控制原语)
        1. [Create Primitive](#create-primitive)
        2. [Terminate Primitive](#terminate-primitive)
        3. [Switch Primitive](#switch-primitive)
        4. [Block Primitive](#block-primitive)
        5. [Wake-up Primitive](#wake-up-primitive)
        6. [Suspend Primitive](#suspend-primitive)
        7. [Active Primitive](#active-primitive)
    4. [The Linux Process State Transfer](#the-linux-process-state-transfer)
4. [Process Scheduling进程调度](#process-scheduling进程调度)
    1. [Aims of Scheduling调度的目标](#aims-of-scheduling调度的目标)
    2. [Types of Scheduling调度的种类](#types-of-scheduling调度的种类)
    3. [Criteria of Scheduling调度的准则](#criteria-of-scheduling调度的准则)
    4. [Decision Modes of Scheduling](#decision-modes-of-scheduling)
    5. [Basic Algorithms of Scheduling基本调度算法](#basic-algorithms-of-scheduling基本调度算法)
    6. [Improved Algorithms of Scheduling改进型调度算法](#improved-algorithms-of-scheduling改进型调度算法)
    7. [Real-Time Algorithms of Scheduling实时调度算法](#real-time-algorithms-of-scheduling实时调度算法)
5. [Process Concurrency Control, Communication and Deadlock并发控制、进程通信与死锁](#process-concurrency-control-communication-and-deadlock并发控制-进程通信与死锁)
    1. [Why and What Concurrency Control](#why-and-what-concurrency-control)
    2. [process Interactions](#process-interactions)
    3. [Critical临界区](#critical临界区)
    4. [Semaphore信号量](#semaphore信号量)
    5. [Monitor管程](#monitor管程)
    6. [Message Passing消息传递](#message-passing消息传递)
    7. [Resource](#resource)
    8. [Deadlock死锁](#deadlock死锁)
        1. [What is Deadlock](#what-is-deadlock)
        2. [Deadlock Prevention](#deadlock-prevention)
        3. [Deadlock Avoidance](#deadlock-avoidance)
        4. [Deadlock Detection](#deadlock-detection)
        5. [Deadlock Recovery](#deadlock-recovery)
    9. [Classical Problems of Synchronization经典同步问题](#classical-problems-of-synchronization经典同步问题)
        1. [The Readers-Writers Problem读者-写者问题](#the-readers-writers-problem读者-写者问题)
        2. [The Producers-Consumers Problem生产者-消费者问题](#the-producers-consumers-problem生产者-消费者问题)
        3. [The Dining Philosophers Problem哲学家就餐问题](#the-dining-philosophers-problem哲学家就餐问题)
        4. [The Sleeping Barber Problem打瞌睡的理发师问题](#the-sleeping-barber-problem打瞌睡的理发师问题)

<!-- /code_chunk_output -->

## Preface

### Main TOC for Learning

1. 进程和线程
    1. 进程/线程的概念及其产生缘由
    1. 进程/线程的描述及其数据结构
    1. 进程和线程的区别和联系
1. 进程控制与进程状态转换
1. 进程调度与经典调度算法
1. 并发控制与经典同步问题
    1. 互斥、同步和通信
        1. 互斥、同步、通信的定义和关系
        1. 实现互斥、同步、通信的方法
            1. 信号量的定义和使用方法
            1. 管程的定义和使用方法
    1. 死锁
        1. 死锁的定义与死锁产生的必要条件
        1. 死锁的预防、避免、检测和解除
    1. 经典同步问题的类型及其解法

### Keys for Programming

1. how and when create process and thread
1. how to synchronize process and thread to complete a work

## Process and Thread进程与线程

### What is Process进程是什么

1. what is `process`
    1. also called `task`（任务）
    1. a `process` is the **instance** of a computer program that is **being executed** with **one or many threads** (defined by wiki)
1. what `process` contains
    1. program: algorithm + data structure
    1. data
    1. `PCB`(Process Control Block, 进程控制块)
1. what is `process image`（进程映像）
    1. a process image is **a copy** of a given process's state at **a given point in time** (defined by wiki)
    1. collection of program, data, stack, and attributes(`PCB`)
1. characteristics of `process`
    + dynamic（动态性）
    + independent（独立性）
    + concurrency（并发性）
    + asynchronous（异步性）
1. what is pre-conditions of concurrency: `Bernstein Condition`(processes cannot execute in parallel if one effects values used by the other.)

### Process Description进程描述

1. Tables are constructed for each entity the operating system manages
    + process table
    + memory table
    + IO table
    + file table
1. `PCB` contains:
    1. identifiers标识符
        + unique numeric identifier
            + this process
            + parent process
        + user identifier
    1. state information状态信息
        + contents of processor registers: user-visible registers, control and status registers, stack pointers
        + `PSW`(program status word): status information(EFLAGS register)
    1. control information控制信息
        + scheduling and state information
        + data structuring (e.g., parent-child relationships; membership in wait/ready queues)
        + inter-process communication
        + process privileges
        + memory management
        + resource ownership and utilization

### What is Tread线程是什么

1. what is `thread`: the smallest sequence of programmed instructions that can be managed independently by a scheduler(defined by wiki)
1. why introduce `thread`
    + `process` is too heavy to create, switch and terminate in one application/job
    + ( :warning: **unconfirmed** ) make use of `multi-core processor`
1. `process` vs `thread`
    + `process` is resource allocate unit
    + `thread` is schedule unit
1. types of `thread`
    1. `(pure) kernel-level thread`（纯）内核级线程
    1. `(pure) user-level thread`（纯）用户级线程
    1. `combined-level thread`混合型线程

### Thread Description线程描述

1. resource owned by `thread`
    1. `TCB`
    1. stack
1. `TCB` contains
    1. identifier标识符
    1. state information状态信息
        + general registers通用寄存器
        + `PC`程序计数器
        + `PSW`程序状态寄存器
        + `SP`栈指针寄存器

## Process Control进程控制

### What is Process Control进程控制是什么

1. `process control` is the same as `process states control`
1. `process switch` is also known as `context switch`
1. **`primitive`** and **`kernel`** : a primitive is the smallest 'unit of processing' available to a programmer of a given machine, or can be an atomic element of an expression in a language
    + switch
    + create and terminate
    + block and wake-up
    + suspend and activate
1. `process switch` and `mode switch`
    + **`process switch`** : save one process’s execution context & restore that of another process, is done by the kernel (scheduler)
    + **`mode switch`** : change processor execution mode from one privilege level to another
    + `process switch` will result in `mode switch`, **not the other way（反之则不然）**
1. `process tree`

### Process Control and States Transfer进程控制与进程状态转换

1. `process` states
    + 2-states: `running`, `non-running`
    + 3-states: `running`, `ready`, `blocked`
    + 5-states: `new/create`, `running`, `ready`, `blocked`, `exit/terminated`
    + 7-states: `new/create`, `running`, `ready`, `ready-suspended`, `blocked`, `blocked-suspended`, `exit/terminated`
1. `process` states transfer = > lack of resource
    + `running` or not: waiting processor
    + `blocked` or not: waiting event
    + `suspended` or not: waiting memory
1. some questions
    + can `blocked-suspended` directly transfer to `ready` :question: = = > `blocked-suspended` directly transfer to `ready` would change two states which belong to difference scheduling types( medium-term scheduling && short-term scheduling )
    + relationship of `suspend` and `swapping` :question: = > `suspend` use `swapping`
    + all memory swap out to storage when a process `suspended` :question: = > yes, except for `PCB`

:warning: `ready-suspended` and `blocked-suspended` only used in those `OS` which without `virtual memory`

![scheduling levels](./assets_diagram/scheduling_leves.svg)

### Process Control Primitives进程控制原语

#### Create Primitive

1. when to create process
    + user login
    + job submit
    + service start when a process request
    + process create a new process
1. what to do while creating a process
    1. assign a unique process identifier
    1. allocate space for the process
    1. initialize process control block
    1. set up appropriate linkages: add new process to linked list used as a scheduling queue
    1. others: maintain an accounting file

#### Terminate Primitive

1. when to terminate a process
    + user logout
    + `halt` instruction when `process` executing
    + error or fault conditions
    + `quit` an application
1. what to do while terminating a process
    1. find && read `PCB`
    1. terminate process
    1. terminate descendant process
    1. return resource
    1. remove from queue && waiting system clean up

#### Switch Primitive

1. `process switch`
    + also known as `context switch`
    + `context switch` is the process of storing the state of a process or thread
1. when to switch a process
    + interrupts
        + clock interrupt
        + IO interrupt
    + memory fault
    + trap
    + supervisor call: such as file open
1. what to do while switching a process
    1. handle current process
        + save execution context
        + update `PCB`
        + move `PCB` to appropriate queue
    1. handle another process
        + select another process for next execution from ready queue
        + update `PCB`
        + restore execution context

#### Block Primitive

#### Wake-up Primitive

#### Suspend Primitive

#### Active Primitive

### The Linux Process State Transfer

1. `R` : running or runnable (on run queue)
1. `D` : uninterruptible sleep (usually IO)
1. `S` : interruptible sleep (waiting for an event to complete)
1. `T` : stopped by job control signal
1. `Z` : defunct ("zombie") process, terminated but not reaped by its parent
1. `t` : stopped by debugger during the tracing
1. `W` : paging (not valid since the 2.6.xx kernel)
1. `X` : dead (should never be seen)

![should never be seen](./assets_diagram/linux_ps_transfer.svg)

## Process Scheduling进程调度

### Aims of Scheduling调度的目标

+ response time响应时间
+ throughput吞吐量
+ efficiency效率
+ fairness/starve公平性/饿死

### Types of Scheduling调度的种类

1. `long-term scheduling`/`job scheduling`长程调度/作业调度
    + decision to add to the pool of processes to be executed
    + create and exit
1. `medium-term scheduling`/`swapping`中程调度/交换
    + decision to add to the number of processes that are partially or fully in main memory
    + ready-suspended and blocked-suspended
1. `short-term scheduling`/`process scheduling`/`context scheduling`短程调度/进程调度/上下文调度
    + decision to which available process/thread will be executed by the processor
    + running, ready and blocked

:point_right: one `OS` do not need all the three types of scheduling具体的`OS`不必都具备三种调度

### Criteria of Scheduling调度的准则

1. user-oriented用户角度
    1. user-oriented, performance-related用户角度、性能相关
        + **response time响应时间** : elapsed time between the submission of a request until there is output (only has output, not finish)
        + **turnaround time周转时间** : **the total time** taken between the submission of a program/process/thread/task for execution and the return of the complete output to the customer/user
        + **deadlines截止时间**
            + **starting deadline开始截止时间**
            + **completion deadline完成截止时间**
        + **priority优先权**
    1. user-oriented, others
        + **predictability可预测性** : run in about the same amount of time and at about the same cost regardless of the load on the system无论系统上的负载如何，都可以在大约相同的时间内以大约相同的成本运行完毕
1. system-oriented系统角度
    1. system-oriented, performance-related系统角度、性能相关
        + **throughput吞吐量** : the number of processes completed per unit of time(depends on the average length of a process)
        + **processor utilization处理机利用率** : the percentage of time that the processor is busy
    1. system-oriented, others
        + **fairness公平性** : processes should be treated the same, and no process should suffer starvation
        + **enforcing priorities强制优先权** : higher-priority, more processor time
        + **balancing resources资源平衡** : keep the resources of the system busy

### Decision Modes of Scheduling

+ **`non-preemptive`（不可剥夺式/非抢占式）** : once a process is in the running state, it will continue until it terminates or blocks itself
+ **`preemptive`（可剥夺式/抢占式）** : the currently running process may be interrupted and moved to the ready state

### Basic Algorithms of Scheduling基本调度算法

1. **`FCFS`(First Come First Served，先来先服务)**
1. **`SPF`(Shortest Process First，短进程优先)/SPN`(Shortest Process Next)**
1. **`SRTF`(Shortest Remaining Time First，最短剩余时间优先)**
1. **`HRRN`(Highest Response Ratio Next，高响应比优先)** ： 既照顾短进程，又考虑进程到达的先后次序，同是不会使长进程饥饿
    + response ratio = (w + s) / s, w: waiting time, s: serve time
    + characteristics with `SPF` and `FCFS`
        + 如果进程的等待时间相同，则要求服务时间越短优先权越高，因此，具有`SPF`的特点
        + 如果进程的要求服务时间相同，则等待时间越长优先权越高，因此，具有`FCFS`的特点
    + 进程的优先权随等待时间的增加而提高，因此，对于长进程，当等待时间足够长时，其优先权得到提升，从而不会产生长进程饥饿现象
1. **`RR`(Round Robin，时间片轮转)**: 保证就绪队列中的所有进程，在一个给定的时间内，均能获得时间片的运行时间，即，系统能在给定的时间内响应所有用户的请求
1. **`FB`(FeedBack，反馈算法)**

### Improved Algorithms of Scheduling改进型调度算法

1. **`MFQ`(Multilevel Feedback Queue，多级反馈队列)** ： combined `RR`, `FCFS` with `FB` and based on priority
1. **`VRR`(Virtual Round Robin，虚拟时间片轮转)** ： 改进`RR`不利于`IO-intensive process`
    1. 进程由于时间片用完，进入就绪队列
    1. 进程由于等待某个事件放弃`CPU`进入阻塞队列，当等待的事件发生后，该进程进入辅助队列
    1. 首先从辅助队列中调度进程，当辅助队列为空时，才从就绪队列中调度进程

### Real-Time Algorithms of Scheduling实时调度算法

1. what is real-time system: correctness depends on **not only the result** , **but also the time**
1. types of real-time system
    1. by the type of business
        1. real-time control system（实时控制系统）
        1. real-time information processing system（实时信息处理系统）
    1. by the critical of deadline
        1. hard real-time（硬实时）
        1. soft real-time（软实时）
    1. by the periodic of business(is this perspective correct :question:)
        1. periodic（周期性）
        1. aperiodic（非周期性）
1. characterized requirements of real-time system
    1. determinism（确定性）
    1. responsiveness（响应性）
    1. user control（用户控制）
    1. reliability（可靠性）/fail-soft operation（故障弱化）
1. features of real-time system
    1. fast context switch
    1. small size
    1. ability to respond to external interrupts quickly
    1. multitasking with inter-process communication tools such as semaphores, signals and events
    1. files that accumulate data at a fast rate, use of special sequential files that can accumulate data at a fast rate(sort by which rule?)
    1. preemptive scheduling base on priority
    1. minimization of intervals during which interrupts are disabled
    1. delay tasks for fixed amount of time
    1. special alarms and timeouts
1. basic policies of real-time process scheduling
    1. `RRPS`(Round Robin Preemptive Scheduler)
        + respond time: in second-level
    1. `PNS`(Priority-driven Non-preemptive Scheduler)
    1. `PPS`(Priority-driven Preemptive Scheduler)
    1. `IPS`(Immediate Preemptive Scheduler)
1. aims of real-time scheduling: finish when deadline comes, fairness and response time is not the most important
1. classes of real-time scheduling algorithms:
    1. static table-driven（静态表驱动调度法）
    1. static priority-driven preemptive（静态优先级剥夺调度法）
    1. dynamic planning-based（动态计划调度法）
    1. dynamic best effort（动态最大努力调度法）
1. deadline-based scheduling
    + information used
        + ready time
        + starting deadline
        + completion deadline
        + processing time
        + resource requirements
        + priority
        + subtask scheduler
    + key issues:
        1. which task to schedule next?
        1. what sort of preemption is allowed?
    + algorithms
        1. `EDF`(Earliest Deadline First，最早截止时间优先)
        1. `EDUIT`(Earliest Deadline with Unforced Idle Times，允许CPU空闲的EDF调度算法)
1. `RMS`(Rate Monotonic Scheduling，速率单调调度)
    + for **periodic tasks**
    + assigns priorities based on their periods: **shorter period higher priority** ==> that is why it called **"Monotonic"**

## Process Concurrency Control, Communication and Deadlock并发控制、进程通信与死锁

### Why and What Concurrency Control

1. **interactions** : sharing and communication共享与通信
1. **risks** : deadlock and starvation死锁与饥饿
1. **requirements and solutions** :
    1. mutual exclusion互斥
        1. software approaches/application approaches：
            + 通过共享内存变量，进程独立完成互斥
            + 因进程数不同情景不同，因此算法不具备通用性（只能应用于两个进程间的互斥？）
            + 关键问题： 怎样保证原子性 :question:
        1. hardware support: 轮询机制
            + 屏蔽中断
            + 专用机器指令： `TS`、 `swap/exchange` ==> 功能一样，具体的实现和使用有区别
        1. :star2: semaphore: 事件队列机制（分散式）
        1. monitor： 集中仲裁机制（集中式）
        1. message passing（semaphore的进阶版）
    1. synchronization同步
        1. :star2: semaphore信号量
        1. monitor管程
    1. `IPC(Inter-Process Communication)`进程间通信/`LPC(Local-Process Communication)`本地进程通信
        1. shared memory共享内存/shared storage共享存储
        1. pipeline管道
        1. message passing消息传递
        1. semaphore信号量（主要用于同步）
    1. `RPC(Remote Procedure Communication)`远程进程间通信
        1. `socket`
        1. `web service`(based on `HTTP`/`HTTPS`)
    1. `deadlock`
        1. deadlock prevention : 
        1. deadlock avoidance : 
        1. deadlock detection : 
        1. deadlock recovery : 
1. **root causes** :
    1. limited resources资源有限
    1. need cooperation需要协作
1. **keys** :
    1. `primitive`: who && how
    1. `notification`: check or event-driven

### process Interactions

```math
\text{mutual exclusion} \subset synchronization \subset communication
```

1. **mutual exclusion** : 一次只能一个进程访问
    + processes unaware of each other进程不知道对方的存在
    + competition竞争
    + mutual exclusion, deadlock, starvation
1. **synchronization** : 又称直接制约关系，指多个进程为了合作完成任务，必须严格按照规定的 **某种先后次序** 来运行
    + processes indirectly aware of each other进程间接地知道对方的存在
    + cooperation by sharing通过共享进行协作
    + mutual exclusion, deadlock, starvation, data coherence
1. **communication** :
    + process directly aware of each other进程直接地知道对方的存在
    + cooperation by communication通过通信进行协作
    + deadlock, starvation
1. **mutual exclusion** vs **synchronization** : 同步中包含了/实现了互斥，同步是更为复杂的互斥，互斥是特殊的同步
    + 互斥是指某一资源同时只允许一个访问者对其进行访问，具有唯一性和排它性。互斥不会限制访问者对资源的访问顺序，即访问是无序的
    + 同步是指在互斥的基础上（大多数情况），通过其它机制实现访问者对资源的有序访问
1. **synchronization** vs **communication** : 同步是一种特殊的/间接的通信

> :computer: 同步与互斥的区别: <https://songlee24.github.io/2015/04/29/linux-syn-mut-difference/>
> :computer: 深入浅出RPC原理： <https://ketao1989.github.io/2016/12/10/rpc-theory-in-action/>

### Critical临界区

1. `critical resource`: 一次仅允许一个进程使用的资源
1. `critical section`: 需互斥访问`critical resource`的那段程序指令
1. critical resource entry criterions = = > 好的互斥控制机制的标准
    1. 空闲让进
    1. 忙则等待
    1. 有限等待
    1. 让权等待

### Semaphore信号量

### Monitor管程

### Message Passing消息传递

### Resource

1. reusable resource可重用资源
1. consumable resource易耗资源/可消耗资源: interrupts, signals, messages, information in IO buffers, et al...

### Deadlock死锁

#### What is Deadlock

#### Deadlock Prevention

|Condition|Prevent Approach|
|--|--|
|mutual exclusion|SPOOLing everything|
|hold and wait|request all resources initially|
|no preemption|take resources away|
|circular wait|order resources numerically|

#### Deadlock Avoidance

#### Deadlock Detection

#### Deadlock Recovery

### Classical Problems of Synchronization经典同步问题

#### The Readers-Writers Problem读者-写者问题

单资源互斥问题

#### The Producers-Consumers Problem生产者-消费者问题

进程同步问题

#### The Dining Philosophers Problem哲学家就餐问题

多资源互斥问题

#### The Sleeping Barber Problem打瞌睡的理发师问题

进程同步问题
