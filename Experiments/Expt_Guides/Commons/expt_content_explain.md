
1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”

:exclamation::exclamation::exclamation:本实验内容指引下每节分成三部分:exclamation::exclamation::exclamation:

1. **相关操作与命令讲解** ==> 实验涉及的操作与命令的讲解
1. **:microscope: 实验具体内容：** ==> 实验的具体步骤
1. **参考结果截图** ==> 实验的参考结果截图，**实验报告应包含类似的截图**
