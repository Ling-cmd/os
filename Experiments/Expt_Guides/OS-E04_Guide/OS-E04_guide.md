# OS-E04 实验项目04： Bash脚本基础编程

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
3. [实验预备知识](#实验预备知识)
4. [实验注意与说明](#实验注意与说明)
5. [实验内容与实验指引](#实验内容与实验指引)
    1. [实验内容01： 安装nano编辑器与编写"Hello World"脚本](#实验内容01-安装nano编辑器与编写hello-world脚本)
        1. [:microscope: 实验内容01具体任务](#microscope-实验内容01具体任务)
        2. [:ticket: 实验01结果参考截图](#ticket-实验01结果参考截图)
        3. [:bookmark: 实验内容01相关知识](#bookmark-实验内容01相关知识)
            1. [UNIX-like常用编辑器](#unix-like常用编辑器)
            2. [bash脚本及其运行](#bash脚本及其运行)
    2. [实验内容02： bash变量与基本控制结构](#实验内容02-bash变量与基本控制结构)
        1. [:microscope: 实验内容02具体任务](#microscope-实验内容02具体任务)
        2. [:ticket: 实验02结果参考截图](#ticket-实验02结果参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)
            1. [bash变量](#bash变量)
            2. [bash的判断](#bash的判断)
            3. [bash的分支控制](#bash的分支控制)
            4. [bash的循环控制](#bash的循环控制)
6. [FAQ](#faq)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：6
1. 实验目的与要求：
    1. 掌握`bash`脚本的基本编写方法和运行方法
    1. 掌握`bash`脚本的变量
    1. 掌握`bash`脚本的基本控制结构
1. 实验内容：
    1. 安装nano编辑器与编写"Hello World"脚本
    1. bash变量与基本控制结构
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`VirtualBox` + `CentOS`

## 实验前置条件

`Linux`环境： 可以基于虚拟机环境下安装的`Linux`，也可基于`Windows 10 WSL`环境下安装的`Linux`，也可基于裸机环境下安装的`Linux`，也可基于云服务器环境下安装的`Linux`。

## 实验预备知识

请阅读以下内容：

1. [阮一峰： Bash 脚本教程](http://www.ruanyifeng.com/blog/2020/04/bash-tutorial.html)
1. [鸟哥的Linux私房菜： 第十章、认识与学习Bash](http://linux.vbird.org/linux_basic/0320bash.php)
1. [鸟哥的Linux私房菜： 第十二章、学习shell scripts](http://linux.vbird.org/linux_basic/0340bashshell-scripts.php)

## 实验注意与说明

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验结果参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验内容与实验指引

### 实验内容01： 安装nano编辑器与编写"Hello World"脚本

#### :microscope: 实验内容01具体任务

1. 安装`nano`编辑器： `yum install nano`
1. 新建一个名为`OS-E04`的目录并进入该目录: `mkdir OS-E04 && cd OS-E04`
1. 使用`nano`新建并编辑保存`helloworld.sh`脚本文件: `nano helloworld.sh`，该脚本文件至少输出`hello world! I am <EN_and_FN_in_PY>`（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
1. 运行该脚本文件
1. 请将脚本源代码和脚本运行结果截图

#### :ticket: 实验01结果参考截图

![helloworld](./assets_image/helloworld.png)

#### :bookmark: 实验内容01相关知识

##### UNIX-like常用编辑器

`UNIX-like`两大编辑器是`Vi/Vim`（`Vim`是`Vi`的升级替换版，目前的`Vi`一般均指`Vim`）和`Emacs`（其中一般`distro`中默认`Vim`），两个编辑器都有众多的支持者，并由此引发了自1985起的旷日持久的`Editor War`（可在互联网上搜索 **"Editor War"** 或 **"编辑器之战"** 了解更多）。

`Vim`和`Emacs`功能强大，但入门门槛高、学习曲线陡，两者的使用方法也完全不同。

`GNU nano`是除`Vim`和`Emacs`之外使用较多的编辑器，`nano`与`Windows`下的`记事本`一样简单、无入门槛（功能和性能远远强大于`记事本`，但远远不如`Vim`和`Emacs`）。

> read more: [Emacs, Nano, or Vim: Choose your Terminal-Based Text Editor Wisely](https://medium.com/linode-cube/emacs-nano-or-vim-choose-your-terminal-based-text-editor-wisely-8f3826c92a68)

##### bash脚本及其运行

规范的shell脚本的第一行会指出由哪个解释器来执行脚本中的内容，在`bash`中一般为：

```bash{.line-numbers}
#!/usr/bin/env bash
或
#!/bin/bash
或
#!/bin/sh
```

:point_right: `sh`为`bash`的软链接，推荐采用写法`#!/usr/bin/env bash`或`#!/bin/bash`

shell脚本的执行通常可以用以下三种方式：

1. `bash test.sh`或`sh test.sh`（推荐）： 脚本文件本身没有可执行权限，在子`shell`中执行
1. `./test.sh` （当前路径下执行脚本）： 脚本文件需有可执行权限（使用`chmod`添加可执行权限），在子`shell`中执行
1. `source test.sh`或`. test.sh`： 脚本文件需有可执行权限（使用`chmod`添加可执行权限），在当前`shell`执行

### 实验内容02： bash变量与基本控制结构

#### :microscope: 实验内容02具体任务

1. 设计一个名为`lc.sh`的脚本，该脚本的逻辑如下：
    1. 读取用户输入的字符串
    1. 如果用户输入的字符串为"exit"则退出脚本运行
    1. 如果是普通文件名，则输出该普通文件的内容
    1. 如果是目录文件名，则以`ls -l`方式列出该目录文件的子文件
    1. 不断循环，直至用户输入的字符串为"exit"
1. 对`lc.sh`脚本进行编码： `nano lc.sh`
1. 运行`lc.sh`脚本： `sh lc.sh`
1. 请将脚本源代码和脚本运行结果截图

#### :ticket: 实验02结果参考截图

![lc](./assets_image/lc.png)

#### :bookmark: 实验内容02相关知识

##### bash变量

定义变量时，变量名不加`$`符号，使用变量的值时加`$`符号，即变量名为左值、`$`符号加变量名为右值。

:warning: :warning: :warning:定义变量时，赋值符左右不能有空格。

未定义的变量的值为空。

变量名的命名须遵循如下规则：

1. 首个字符必须为字母（a-z，A-Z）
1. 中间不能有空格，可以使用下划线（_）
1. 不能使用标点符号
1. 不能使用`bash`里的关键字（可用`help`命令查看保留关键字）

```bash{.line-numbers}
a_variable="123"

echo $a_variable

# '{' and '}' is optional, but is useful for human read and interpreter execute
echo ${a_variable}
echo "here is ${a_variable}456"
```

##### bash的判断

`bash`使用`test`或`[  ]`对条件进行判断。

:warning: `[   ]`左右必须有空格字符。

`bash`支持的判断如下（完整的列表可通过`man test`查询）：

1. 数值判断
    1. `-eq`(equal)：相等则为真
    1. `-ne`(not equal)： 不相等则为真
    1. `-gt`(greater than): 大于则为真
    1. `-ge`(greater than or equal)： 大于或等于则为真
    1. `-lt`(less than)： 小于则为真
    1. `-le`(less than or equal)： 小于或等于则为真
1. 字符串判断
    1. `==`或`=`： 等于则为真
    1. `!=`： 不相等则为真
    1. `-z "string"`(zero) ： 字符串长度为零则为真
    1. `-n "string"`(non-zero)： 字符串长度不为零则为真
1. 文件类型判断
    1. `-f "filename"`： 如果文件存在且为普通文件则为真
    1. `-d "filename"`： 如果文件存在且为目录则为真
    1. `-c "filename"`： 如果文件存在且为字符型特殊文件则为真
    1. `-b "filename"`： 如果文件存在且为块特殊文件则为真
    1. `-S "filename"`： 如果文件存在且为`socket`文件则为真
    1. `-p "filename"`： 如果文件存在且为`FIFO(pipe)`文件则为真
    1. `-L "filename"`： 如果文件存在且为链接文件则为真
1. 文件状态与权限判断
    1. `-e "filename"`(exist)： 如果文件存在则为真
    1. `-s "filename"`： 如果文件存在且非空时则为真
    1. `-r "filename"`： 如果文件存在且可读则为真
    1. `-w "filename"`： 如果文件存在且可写则为真
    1. `-x "filename"`： 如果文件存在且可执行则为真
    1. `-u "filename"`： 如果文件存在且具有`SUID`属性则为真
    1. `-g "filename"`： 如果文件存在且具有`GUID`属性则为真
    1. `-k "filename"`： 如果文件存在且具有`sticky bit`属性则为真
1. 文件间比较
    1. `-nt`(newer than)： 新则为真
    1. `-ot`(older than):  旧则为真
    1. `-ef`(equal file)： 同一个`inode`则为真
1. 逻辑运算
    1. `-a`(and)： 逻辑与
    1. `-o`(or)： 逻辑或
    1. `!`： 逻辑非

```bash{.line-numbers}
num1=100
num2=100
# if [ ${num1} -eq ${num2} ]; then
if test ${num1} -eq ${num2}; then
    echo 'the two number equal'
else
    echo 'the two number not equal'
fi

str1="abc"
str2="abc"
if test ${str1}==${str2}
then
    echo 'the two string equal'
else
    echo 'the two string not equal'
fi

if [ ${num1} -eq ${num2} -a ${str1} == ${str2} ]; then
    echo 'both equal'
else
    echo "not both equal"
fi
```

##### bash的分支控制

```bash{.line-numbers}
# if...elif...else
if condition; then
    command
    ...
    command
elif condition; then
    command
    ...
    command
else
    command
    ...
    command
fi

# case...esac
case ${var} in
case_1)
    command
    ...
    command
    ;;
case_2)
    command
    ...
    command
    ;;
esac
```

##### bash的循环控制

```bash{.line-numbers}
# while loop
while condition
do
    command
done

# until loop
until condition
do
    command
done

# for loop
for ((init;condition;update))
do
    command
done

# for-in loop
for var in item1 item2 ... itemN
do
    command
    ...
    command
done
```

## FAQ

1. [CentOS 7 yum时出现“Could not resolve host: mirrorlist.centos.org: Unknown error”错误]((../OS-Expt_FAQ/OS-expt_faq.md#centos-7-yum时出现could-not-resolve-host-mirrorlistcentosorg-unknown-error错误)) :point_right: [点击这里查看分析与解决](../OS-Expt_FAQ/OS-expt_faq.md#centos-7-yum时出现could-not-resolve-host-mirrorlistcentosorg-unknown-error错误)
