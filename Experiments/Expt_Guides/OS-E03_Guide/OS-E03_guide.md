# OS-E03 实验项目03： Linux的进程操作和文件操作命令

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
3. [实验内容说明](#实验内容说明)
4. [实验内容指引](#实验内容指引)
    1. [实验内容01：Linux文件操作命令](#实验内容01linux文件操作命令)
    2. [实验内容02：Linux作业操作命令](#实验内容02linux作业操作命令)
    3. [实验内容03：Linux进程操作命令](#实验内容03linux进程操作命令)
5. [FAQ](#faq)
6. [延伸阅读](#延伸阅读)
    1. [教程](#教程)
    2. [手册或工具](#手册或工具)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：综合性
1. 实验学时：6
1. 实验目的与要求：
    1. 掌握`Linux`文件操作命令
    1. 掌握`Linux`作业操作命令
    1. 掌握`Linux`进程操作命令
1. 实验内容：
    1. 使用`Linux`文件操作命令： `ls`, `cp`, `mv`, `rm`, `cd`, `pwd`, `mkdir`, `ln`等
    1. 使用`Linux`作业操作命令： `jobs`, `bg`, `fg`, `kill`等
    1. 使用`Linux`进程操作命令： `ps`, `top`等
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Windows` + `VirtualBox` + `CentOS`

## 实验前置条件

`Linux`环境： 可以基于虚拟机环境下安装的`Linux`，也可基于`Windows 10 WSL`环境下安装的`Linux`，也可基于裸机环境下安装的`Linux`，也可基于云服务器环境下安装的`Linux`。

## 实验内容说明

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键

1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”

:exclamation: :exclamation: :exclamation: 本实验内容指引下每节分成三部分 :exclamation: :exclamation: :exclamation:

1. **相关操作与命令讲解** ==> 实验涉及的操作与命令的讲解
1. **:microscope: 实验具体内容：** ==> 实验的具体步骤
1. **参考结果截图** ==> 实验的参考结果截图，**实验报告应包含类似的截图**

<!--
@import "../Commons/expt_content_explain.md"
-->

## 实验内容指引

### 实验内容01：Linux文件操作命令

1. **相关操作与命令讲解：**
    1. `ls`(list)：列出目录内容（子目录和文件）
    1. `ll`(list long)： 以详细方式
    1. `.`文件(点文件)：`UNIX-like`中以`.`开头的文件是隐藏文件/目录
    1. `<TAB>`命令补全： 按下`<TAB>`可进行命令自动补全（当可以自动补全时）
    1. `mkdir`(make directory)： 创建目录
    1. `touch`： 创建文件
    1. `echo`： 输出
    1. `cp`(copy)： 复制文件或目录
    1. `mv`(move)： 移动文件或目录
    1. `rm`(remove)： 删除文件或目录
    1. `cd`(change directory)： 切换当前目录
    1. `pwd`(print working directory)： 打印/输出当前目录
    1. `ln`(link)： 创建一个链接文件（硬链接或软链接）
    1. `alias`： 创建一个别名
    1. `>>`： 输出重定向（追加模式）
1. **:microscope: 实验具体内容：**
    1. 运行 **`pwd`**
    1. 运行 **`mkdir EN_and_FN_in_PY`** （请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行 **`cd EN_and_FN_in_PY`**（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行 **`pwd`**
    1. 运行 **`mkdir home bin src .hide_me`**
    1. 运行 **`touch a_file b_file c_file`**
    1. 运行 **`ls`**
    1. 运行 **`ls -a`**
    1. 运行 **`alias lsa='ls -a'`**
    1. 运行 **`lsa`**
    1. 运行 **`echo hello! My enrollment number and full name is EN_and_FN_in_PY >> d_file`** （请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行 **`cat d_file`**
    1. 运行 **`cd ..`**
    1. 运行 **`pwd`**
    1. 运行`cd ./EN_and_FN_in_PY/home`（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行`pwd`
    1. 运行`cd -`
    1. 运行`pwd`
    1. 运行`cd ./EN_and_FN_in_PY`（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行`mv ./d_file ./home/`
    1. 运行`ln -s ./home/d_file soft-link_to_d_file`
    1. 运行`ll -a`
    1. 运行`cat soft-link_to_d_file`
    1. 运行`cd .. && rm -rf ./EN_and_FN_in_PY`
    1. 运行`ll`

![file_01](./assets_image/file_01.png)

![file_02](./assets_image/file_02.png)

### 实验内容02：Linux作业操作命令

1. **相关操作与命令讲解：**
    1. `&`： 附加到命令之后，表示在后台运行
    1. `jobs`： 列出当前`shell`的作业及其状态
    1. `fg`(foreground)： 将后台作业调至前台运行
    1. `bg`(background)： 将后台暂停作业调至后台运行
    1. `<CTRL> + <z>`(zombie)： 将前台作业调至后台暂停
    1. `<CTRL> + <c>`(cancel)： 将前台作业结束
    1. `kill`： 中止进程或作业（中止作业时，作业号前加`%`）
    1. `yes`： 循环输出字符串直至被中止
1. **:microscope: 实验具体内容：**
    1. 运行`yes EN_and_FN_in_PY`（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 按下`<CTRL> + <z>`
    1. 运行`jobs`
    1. 运行`fg %1`（`1`是`yes EN_and_FN_in_PY`对应的作业号，如对应的作业号不是`1`请更改成实际对应的作业号）
    1. 按下`<CTRL> + <c>`
    1. 运行`jobs`
    1. 运行`yes EN_and_FN_in_PY > /dev/null &`（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. 运行`jobs`
    1. 运行`fg %1`（`1`是`yes EN_and_FN_in_PY > /dev/null &`对应的作业号，如对应的作业号不是`1`请更改成实际对应的作业号）
    1. 按下`<CTRL> + <z>`
    1. 运行`bg %1`（`1`是`yes EN_and_FN_in_PY > /dev/null &`对应的作业号，如对应的作业号不是`1`请更改成实际对应的作业号）
    1. 运行`jobs`
    1. 运行`kill %1`（`1`是`yes EN_and_FN_in_PY > /dev/null &`对应的作业号，如对应的作业号不是`1`请更改成实际对应的作业号）
    1. 运行`jobs`

![job_01](./assets_image/job_01.png)

![job_02](./assets_image/job_02.png)

![job_03](./assets_image/job_03.png)

![job_04](./assets_image/job_04.png)

### 实验内容03：Linux进程操作命令

1. **相关操作与命令讲解：**
    1. `ps`(process status)： 列出进程状态
    1. `top`： 动态地显示进程状态
    1. `|`： pipeline管道符，将前一个命令的输出作为另一个命令的输入
1. **:microscope: 实验具体内容：**
    1. 运行`ps -aux | less`
    1. 按下`<q>`
    1. 运行`top`
    1. 按下`<q>`

![ps_01](./assets_image/ps_01.png)

![ps_02](./assets_image/ps_02.png)

## FAQ

请参见[FAQ常见问题集](../OS-Expt_FAQ/OS-expt_faq.md)

## 延伸阅读

### 教程

1. 鸟哥的Linux私房菜 - 第六章： <http://linux.vbird.org/linux_basic/0220filemanager.php>
1. 鸟哥的Linux私房菜 - 第十六章：<http://linux.vbird.org/linux_basic/0440processcontrol.php>
1. UNIX目录结构的来历： <http://www.ruanyifeng.com/blog/2012/02/a_history_of_unix_directory_structure.html>
1. Linux思维导图整理： <https://www.jianshu.com/p/59f759207862>

### 手册或工具

1. Linux命令大全： <https://man.linuxde.net/>
1. Linux命令大全(手册)： <https://www.linuxcool.com/>
