# 实验项目01：虚拟机与Linux的安装与使用

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
    1. [实验环境准备](#实验环境准备)
    2. [实验知识准备](#实验知识准备)
        1. [虚拟机基础知识](#虚拟机基础知识)
        2. [`Linux`基础知识](#linux基础知识)
3. [实验内容指引](#实验内容指引)
    1. [实验内容01： 安装`VirtualBox`](#实验内容01-安装virtualbox)
        1. [`VirtualBox`安装过程](#virtualbox安装过程)
        2. [完成`VirtualBox`安装](#完成virtualbox安装)
    2. [实验内容02： 在`VirtualBox`中安装`CentOS`](#实验内容02-在virtualbox中安装centos)
        1. [`VirtualBox`新建虚拟机](#virtualbox新建虚拟机)
        2. [`VirtualBox`装载`CentOS`镜像安装包](#virtualbox装载centos镜像安装包)
        3. [启动虚拟机安装`CentOS`](#启动虚拟机安装centos)
        4. [完成`CentOS`安装](#完成centos安装)
        5. [启动`CentOS`登录界面](#启动centos登录界面)
4. [实验报告应包含的内容](#实验报告应包含的内容)
5. [延伸阅读](#延伸阅读)
6. [实验说明类FAQ](#实验说明类faq)
    1. [`MacOS`系统如何做本次实验？](#macos系统如何做本次实验)
    2. [是不是必须得使用`VirtualBox`作为`VMM`进行本实验？](#是不是必须得使用virtualbox作为vmm进行本实验)
    3. [实验过程中，忘记截图，怎么办？](#实验过程中忘记截图怎么办)
7. [实验过程非故障类FAQ](#实验过程非故障类faq)
    1. [`VirtualBox`安装过程中，出现对话框“您想安装这个设备软件吗？名称：Oracle Corporation通用串行总线控制器”，是否应该安装](#virtualbox安装过程中出现对话框您想安装这个设备软件吗名称oracle-corporation通用串行总线控制器是否应该安装)
    2. [`VirtualBox`虚拟机运行过程中，鼠标进入虚拟机后怎样退出虚拟机？](#virtualbox虚拟机运行过程中鼠标进入虚拟机后怎样退出虚拟机)
    3. [`CentOS`安装过程中选择“简体中文”，但`CentOS`启动是“英文”界面](#centos安装过程中选择简体中文但centos启动是英文界面)
    4. [`CentOS`安装完成后，启动进入`CLI`登录界面时不知道怎样登录](#centos安装完成后启动进入cli登录界面时不知道怎样登录)
8. [`VirtualBox`安装与启动故障类FAQ](#virtualbox安装与启动故障类faq)
    1. [安装`VirtualBox`时，出现错误对话框：“Installation failed!Error:安装时发生严重错误”](#安装virtualbox时出现错误对话框installation-failederror安装时发生严重错误)
    2. [启动`VirtualBox`时，出现错误对话框：“创建VirtualBoxClient COM对象失败”](#启动virtualbox时出现错误对话框创建virtualboxclient-com对象失败)
9. [`VirtualBox`创建虚拟机故障类FAQ](#virtualbox创建虚拟机故障类faq)
    1. [`VirtualBox`新建虚拟机时只能选择32-bit操作系统？](#virtualbox新建虚拟机时只能选择32-bit操作系统)
    2. [`VirtualBox`新建虚拟机时，出现错误对话框：“无法在父文件夹...请检查父文件夹是否真的存在...”](#virtualbox新建虚拟机时出现错误对话框无法在父文件夹请检查父文件夹是否真的存在)
    3. [`VirtualBox`打开虚拟机配置时，不能点击“OK”且出现“发现无效设置”提醒](#virtualbox打开虚拟机配置时不能点击ok且出现发现无效设置提醒)
    4. [`VirtualBox`启动虚拟机时，出现错误对话框：“不能为虚拟电脑XXX打开一个新任务”](#virtualbox启动虚拟机时出现错误对话框不能为虚拟电脑xxx打开一个新任务)
    5. [`VirtualBox`运行过程中，出现错误对话框：“0x00000000指令引用的0x00000000内存该内存不能为written”](#virtualbox运行过程中出现错误对话框0x00000000指令引用的0x00000000内存该内存不能为written)
10. [`CentOS`安装故障类FAQ](#centos安装故障类faq)
    1. [启动虚拟机安装`CentOS`时，虚拟机界面出现“FATAL: No bootable medium found! System halted”](#启动虚拟机安装centos时虚拟机界面出现fatal-no-bootable-medium-found-system-halted)
    2. [启动虚拟机安装`CentOS`时，出现故障对话框：“...make sure there is enough free space on the disk and that the disk is working properly...”](#启动虚拟机安装centos时出现故障对话框make-sure-there-is-enough-free-space-on-the-disk-and-that-the-disk-is-working-properly)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：综合性
1. 实验学时：6
1. 实验目的与要求：
    1. 了解`Linux`操作系统的发行版本
    1. 掌握`VirtualBox`的安装方法
    1. 掌握`CentOS`的安装方法和安装过程
1. 实验内容：
    1. 实验内容01：安装`VirtualBox`
    1. 实验内容02：在`VirtualBox`中安装`CentOS`
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Windows` + `VirtualBox` + `CentOS`

## 实验前置条件

### 实验环境准备

1. `PC`：一台安装有`Windows 7`或以上版本的`PC`
1. `VirtualBox`安装包： 下载地址 https://download.virtualbox.org/virtualbox/6.1.4/VirtualBox-6.1.4-136177-Win.exe
1. `CentOS` ISO镜像安装包（**以下两种下载方式选择其中一种即可**）：
    1. 直接下载： http://mirrors.aliyun.com/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso
    1. BT下载： http://mirrors.aliyun.com/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1908.torrent

### 实验知识准备

#### 虚拟机基础知识

1. **`虚拟化技术（Virtualization Technology, VT）`：** 在无上下文情况下，一般指`硬件虚拟化（Hardware virtualization）`是一种对计算机或操作系统的虚拟，虚拟化对用户隐藏了真实的计算机硬件，表现出另一个抽象计算平台。
    1. `Intel VT-x(Intel Virtualization Technology)`： `Intel CPU`的`VT`技术
    1. `AMD SVM(AMD Secure Virtual Machine)`： `AMD CPU`的`VT`技术
1. **`虚拟化管理程序（Virtualization Hypervisor）`：** 或称为`虚拟机监视器（Virtual Machine Monitor, VMM）`，是用来创建与运行 **硬件虚拟化机器** 的软件、固件或硬件，典型的`Hypervisor`有`VMWare`、`VirtualBox`、`Xen`、`KVM`、`Parallels`等
1. **`VirtualBox`：** 由德国`InnoTek`软件公司出品`Open Source Software, OSS（开源软件）`的一种`Hypervisor`，现在则由`Oracle`公司进行开发（2008年02月`SUN`收购`InnoTek`，2010年01月`Oracle`收购`SUN`），目前是`Oracle`公司`xVM`虚拟化平台技术的一部分
1. **`Virtual Machine, VM`（虚拟机）：** 是一种计算机系统的仿真器，通过软件模拟具有完整硬件系统功能的、运行在一个完全隔离环境中的完整计算机系统
1. **`Host OS`（宿主系统）：** 运行`Hypervisor`所在的`OS`
1. **`Guest OS`（客机系统）：**  由`Hypervisor`创建的`VM`上运行的`OS`

#### `Linux`基础知识

1. **`POSIX`（可移植操作系统接口）：** 全称`Portable Operating System Interface`（`X`后缀表明其对`Unix API`的传承），是`IEEE`为要在各种`UNIX`操作系统上运行软件，而定义`API`的一系列互相关联的标准的总称
1. **`Linux`：**
    + 一种`Free and Open Source Software, FOSS（自由且开源软件）`的 `Unix-Like` 操作系统内核，**`Linux`本身只是一个`OS`内核**
    + `Linux`基本上逐步实现了`POSIX`兼容，但 **没有参加正式的** `POSIX`认证（**基本上可认为`Linux`是一种`POSIX`系统**）
1. **`Linux Distribution, Linux Distro`（Linux发行版）：** 为一般用户预先集成好的`Linux`及各种应用软件的发行安装版，最流行的`Linux Distro`有：
    + `RPM-based Distribution`： 以`Fedora`, `RHEL`, `CentOS`为代表
    + `Debian-based Distribution`： 以`Debian`, `Ubuntu`为代表
1. **`CentOS`：** 全称`Community Enterprise Operating System`，是一种 **主要在服务器上使用** 的`Linux Distro`，它是来自于`Red Hat Enterprise Linux（RHEL）`依照开放源代码规定发布的源代码所编译而成。由于出自同样的源代码，因此有些要求高度稳定性的服务器以`CentOS`替代商业版的`RHEL`使用。两者的不同，在于`CentOS`并不包含封闭源代码软件。`CentOS`对上游代码的主要修改是为了移除不能自由使用的商标。2014年，`CentOS`组织宣布与`Red Hat`公司合作，但`CentOS`组织将会在新的委员会下继续运作，并不受`RHEL`的影响

## 实验内容指引

### 实验内容01： 安装`VirtualBox`

#### `VirtualBox`安装过程

![01_install_VBox_01](./assets/images/01_install_VBox_01.png)

![01_install_VBox_02](./assets/images/01_install_VBox_02.png)

![01_install_VBox_03](./assets/images/01_install_VBox_03.png)

![01_install_VBox_04](./assets/images/01_install_VBox_04.png)

![01_install_VBox_05](./assets/images/01_install_VBox_05.png)

#### 完成`VirtualBox`安装

![01_install_VBox_06](./assets/images/01_install_VBox_06.png)

### 实验内容02： 在`VirtualBox`中安装`CentOS`

#### `VirtualBox`新建虚拟机

![02_Install_CentOS_00](./assets/images/02_Install_CentOS_00.png)

![02_Install_CentOS_01](./assets/images/02_Install_CentOS_01.png)

![02_Install_CentOS_02](./assets/images/02_Install_CentOS_02.png)

![02_Install_CentOS_03](./assets/images/02_Install_CentOS_03.png)

![02_Install_CentOS_04](./assets/images/02_Install_CentOS_04.png)

![02_Install_CentOS_05](./assets/images/02_Install_CentOS_05.png)

![02_Install_CentOS_06](./assets/images/02_Install_CentOS_06.png)

![02_Install_CentOS_07](./assets/images/02_Install_CentOS_07.png)

#### `VirtualBox`装载`CentOS`镜像安装包

![02_Install_CentOS_08](./assets/images/02_Install_CentOS_08.png)

![02_Install_CentOS_09](./assets/images/02_Install_CentOS_09.png)

![02_Install_CentOS_10](./assets/images/02_Install_CentOS_10.png)

![02_Install_CentOS_11](./assets/images/02_Install_CentOS_11.png)

#### 启动虚拟机安装`CentOS`

![03_Install_CentOS_00-00](./assets/images/03_Install_CentOS_00-00.png)

![03_Install_CentOS_00-01](./assets/images/03_Install_CentOS_00-01.png)

![03_Install_CentOS_01](./assets/images/03_Install_CentOS_01.png)

![03_Install_CentOS_02](./assets/images/03_Install_CentOS_02.png)

![03_Install_CentOS_03](./assets/images/03_Install_CentOS_03.png)

![03_Install_CentOS_04](./assets/images/03_Install_CentOS_04.png)

![03_Install_CentOS_05](./assets/images/03_Install_CentOS_05.png)

![03_Install_CentOS_06](./assets/images/03_Install_CentOS_06.png)

#### 完成`CentOS`安装

![03_Install_CentOS_07](./assets/images/03_Install_CentOS_07.png)

#### 启动`CentOS`登录界面

![03_Install_CentOS_08](./assets/images/03_Install_CentOS_08.png)

![03_Install_CentOS_09](./assets/images/03_Install_CentOS_09.png)

:exclamation: **本实验安装（最小安装）完成后系统没有安装`GUI`界面，只有`CLI`界面，实验过程中不需要使用`GUI`界面**

## 实验报告应包含的内容

请将实验过程截图至实验报告中，实验报告应 **至少** 包含以下内容：

1. 完成`VirtualBox`安装的截图
1. 完成`CentOS`安装的截图
1. 启动`CentOS`的`CLI`登录界面的截图（本实验暂无需登录）

## 延伸阅读

1. :book: :computer: **`Unix`的历史和设计原则：** [《UNIX编程艺术》](http://www.catb.org/~esr/writings/taoup/html/)，[美] Eric S.Raymond 著，姜宏，何源，蔡晓骏 译，电子工业出版社，[豆瓣链接](https://book.douban.com/subject/1467587/)
1. :book: :computer: **`Linux`的历史和设计原则：** [《只是为了好玩 : Linux之父林纳斯自传》](https://github.com/limkokhole/just-for-fun-linus-torvalds)，[芬-美]Linus Torvalds，David Diamond，[豆瓣链接](https://book.douban.com/subject/25930025/)
1. :book: :computer: **`Linux`的经典入门教程：** [《鸟哥的Linux私房菜：基础学习篇（第4版）》](http://linux.vbird.org/linux_basic/)，鸟哥，[豆瓣链接](https://book.douban.com/subject/30359954/)，[GitBook链接](https://legacy.gitbook.com/book/wizardforcel/vbird-linux-basic-4e/details)

## 实验说明类FAQ

### `MacOS`系统如何做本次实验？

1. 方法一：请下载`MacOS`版的`VirtualBox`安装包（<https://download.virtualbox.org/virtualbox/6.1.4/VirtualBox-6.1.4-136177-OSX.dmg>），实验方法一样
1. 方法二：可使用其他`VMM`（如`Parallels Desktop`、`VMware Fusion`）安装`CentOS`

### 是不是必须得使用`VirtualBox`作为`VMM`进行本实验？

不是必须的，可以使用可安装`Linux`的`VMM`，如：`VMware`, `Hyper-V`，也可以使用`WSL(Windows Subsystem for Linux)`，本次实验的目标是熟悉`VMM`的安装与使用（以`VirtualBox`为例）以及`Linux`的安装，为后续实验提供`Linux`环境。

在后续实验中，也可以基于裸机上的`Linux`，云服务器上的`Linux`，`Docker`上的`Linux`进行实验。

### 实验过程中，忘记截图，怎么办？

请至少截取如下两张图：

1. 打开`VirtualBox`后的主界面的截图
1. 启动`CentOS`的`CLI`登录界面的截图

## 实验过程非故障类FAQ

### `VirtualBox`安装过程中，出现对话框“您想安装这个设备软件吗？名称：Oracle Corporation通用串行总线控制器”，是否应该安装

应该安装，“通用串行总结控制器”即`USB Controller`，请勾选`始终信任来“Oracle Corporation”的软件（A）`并点击`安装`

### `VirtualBox`虚拟机运行过程中，鼠标进入虚拟机后怎样退出虚拟机？

按键盘 **右边的CTRL键**

### `CentOS`安装过程中选择“简体中文”，但`CentOS`启动是“英文”界面

本次实验不安装`GUI`，只安装`CLI`，`CLI`为`英文`界面

### `CentOS`安装完成后，启动进入`CLI`登录界面时不知道怎样登录

本次实验不需要登录，登录界面截图即可

如需登录，账号请输入`root`，密码请输入安装过程中设置的密码，**:warning:请注意，密码输入过程不会有任何回显，输完密码后按下`enter`键即可**

## `VirtualBox`安装与启动故障类FAQ

### 安装`VirtualBox`时，出现错误对话框：“Installation failed!Error:安装时发生严重错误”

+ 故障原因：未开启`Device Install Service`和`Device Setup Manager`服务
+ 故障解决：`我的电脑>>>右击>>>管理>>>服务和应用程序>>>服务`，启动`Device Install Service`和`Device Setup Manager`服务

参考链接：

+ <https://blog.csdn.net/ljw124213/article/details/50545101>
+ <https://blog.csdn.net/qq_39689711/article/details/103689092>

### 启动`VirtualBox`时，出现错误对话框：“创建VirtualBoxClient COM对象失败”

+ 故障原因： 兼容性问题
+ 故障解决： 对`VirtualBox`的图标`点击右键>>>属性>>>兼容性`，兼容模式选择不同版本进行尝试

参考链接：<https://blog.csdn.net/danshenxiaobang/article/details/72877363>

## `VirtualBox`创建虚拟机故障类FAQ

### `VirtualBox`新建虚拟机时只能选择32-bit操作系统？

关于`VT`的更多讨论请见： <https://gitee.com/aroming/course_os/issues/I1B4CD>

+ 故障原因：`BIOS`未开启`VT`或其他软件占用`VT`，Win10可从`任务管理器`查看`VT`的启用状态
+ 故障解决：
    1. 进入`BIOS`开启`VT`：根据具体主板而不同
        + `Intel CPU`主板一般为： `Advanced BIOS Features>>>Virtualization`或`Security>>>Virtualization>>>Intel Virtualization Technology`
        + `AMD CPU`主板一般为： `Advanced>>>CPU Configuration>>>SVM Mode`
    1. 关闭其他软件占用`VT`：
        1. `Windows 10`自带的`Hyper-V`：以 **管理员权限** 打开`CMD`或`Windows PowerShell`，输入`bcdedit /set hypervisorlaunchtype off`后重启
        1. `360安全卫士`的`Intel-VT核晶防护引擎`：`360安全卫士>>>安全防护中心>>>安全设置>>>核晶防护`，关闭功能

![OS-E01_P_win10_vt_task_manager](./assets/images/OS-E01_P_win10_vt_task_manager.png)

参考链接： <https://shiyousan.com/post/636245851547291596>

**如果`Windows家庭版`经上面操作仍无法选择64-bit操作系统，请尝试安装`CentOS 32-bit`（ <http://mirrors.huaweicloud.com/centos-altarch/7.7.1908/isos/i386/CentOS-7-i386-Minimal-1908.iso> ）**

### `VirtualBox`新建虚拟机时，出现错误对话框：“无法在父文件夹...请检查父文件夹是否真的存在...”

+ 故障原因：文件路径不正确或对文件夹没有操作权限
+ 故障解决：请将路径更换到其他路径，并确保当前登录账号对文件夹具有读写权限

### `VirtualBox`打开虚拟机配置时，不能点击“OK”且出现“发现无效设置”提醒

![OS-E01_P_no_vt](./assets/images/OS-E01_P_no_vt.png)

+ 故障原因：`BIOS`未开启`VT`
+ 故障解决：进入`BIOS`开启`VT`

### `VirtualBox`启动虚拟机时，出现错误对话框：“不能为虚拟电脑XXX打开一个新任务”

+ 故障原因：需从该错误对话框中的“明细”中进一步确定具体原因
+ 故障解决：
    1. 卸载`VirtualBox`后，重新进行各步骤
    1. 如果“卸载`VirtualBox`后”无法解决，请打开该错误对话框中的“明细”查看各关键字到搜索引擎中查找解决方法

![OS-E01_P_cannot_open_task](./assets/images/OS-E01_P_cannot_open_task.png)

### `VirtualBox`运行过程中，出现错误对话框：“0x00000000指令引用的0x00000000内存该内存不能为written”

+ 故障原因： 多种原因可能造成该故障
+ 故障解决：
    1. 方法一： 卸载`VirtualBox`后，重新进行各步骤
    1. 方法二： 请参考链接（<https://lengjiao.pw/?p=98>）提供的几种方法
    1. 方法三： 尝试关闭安全软件或杀毒软件后重试（偶尔可能是由于安全软件或杀毒软件的原因造成）

## `CentOS`安装故障类FAQ

### 启动虚拟机安装`CentOS`时，虚拟机界面出现“FATAL: No bootable medium found! System halted”

![OS-E01_no_bootable](./assets/images/OS-E01_no_bootable.png)

+ 故障原因：虚拟机启动顺序中没有包含“光驱”（`ISO镜像文件`装载在`虚拟光驱`中）
+ 故障解决：请将启动顺序中“光驱”选项设置为为勾选状态

![OS-E01_set_boot_order](./assets/images/OS-E01_set_boot_order.png)

### 启动虚拟机安装`CentOS`时，出现故障对话框：“...make sure there is enough free space on the disk and that the disk is working properly...”

+ 故障原因：硬盘空间不足
+ 故障解决：请清理硬盘空间，确保有足够的硬盘空间
