# OS  Experiment FAQ

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Linux基础知识类](#linux基础知识类)
    1. [halt（停止）和poweroff（关闭电源）的区别是什么？](#halt停止和poweroff关闭电源的区别是什么)
    2. [Terminal（终端）、CLI（命令行界面）、Shell、TTY（电传打字机）和Console（控制台）的区别是什么？](#terminal终端-cli命令行界面-shell-tty电传打字机和console控制台的区别是什么)
    3. [命令w，who，whoami，who am i的区别是什么？](#命令wwhowhoamiwho-am-i的区别是什么)
    4. [Linux字符界面下如何翻屏？](#linux字符界面下如何翻屏)
2. [CentOS使用类](#centos使用类)
    1. [CentOS 7忘记root密码，怎么办？](#centos-7忘记root密码怎么办)
        1. [方法01： emergency mode](#方法01-emergency-mode)
        2. [方法02： single user mode](#方法02-single-user-mode)
3. [CentOS故障类](#centos故障类)
    1. [CentOS 7 yum时出现“Could not resolve host: mirrorlist.centos.org: Unknown error”错误](#centos-7-yum时出现could-not-resolve-host-mirrorlistcentosorg-unknown-error错误)
        1. [故障分析定位](#故障分析定位)
        2. [故障解决 - 网络不通](#故障解决-网络不通)

<!-- /code_chunk_output -->

## Linux基础知识类

### halt（停止）和poweroff（关闭电源）的区别是什么？

1. 无`ACPI(Advanced Configuration and Power Management Interface)`的系统上：
    + halt关闭`OS`，但没有关闭电源，需要手动再按下"关机"按钮关闭电源
    + poweroff同时关闭`OS`和电源
1. 有`ACPI`的系统上： 没有区别

### Terminal（终端）、CLI（命令行界面）、Shell、TTY（电传打字机）和Console（控制台）的区别是什么？

1. `CLI`： 使用文本命令进行交互的用户界面
1. `Shell`： 一种`CLI`解释器
1. `Terminal` == `TTY`： 一种文本输入输出环境，通过串行线用打印机键盘通过阅读和发送信息的设备
    1. `PTY(Pseudo-TTY)`： 虚拟`TTY`，远程登录或在`GUI`环境下打开`terminal`，`PTY`由`PTMX`和`PTS`组成
        1. `PTMX(Pseudo-Terminal Master)`
        1. `PTS(Pseudo-Terminal Slave)`
1. `Console`： 一种特殊的`Terminal`（输入输出环境与主机一体），早期为系统管理员专用的一种`Terminal`，现在基本与`Terminal`代表同一个事物

即：`CLI`是一种用户界面，`Shell`是一种实现`CLI`的软件，`Terminal`/`TTY`/`Console`在`Shell`和用户之间负责输入/输出的硬件或软件模拟器

更详细请参见：

1. <https://printempw.github.io/the-difference-between-cli-terminal-shell-tty/>
1. <https://unix.stackexchange.com/a/358493>
1. <http://www.freeoa.net/osuport/intronix/linux-tty-ptys-console_3316.html>
1. <https://www.zhihu.com/question/21711307/answer/56056972>
1. <https://www.cnblogs.com/liqiuhao/p/9031803.html>
1. <https://segmentfault.com/a/1190000009082089>

### 命令w，who，whoami，who am i的区别是什么？

1. `w`: show who is logged on in system and what they are doing
1. `who`: show who is logged on in system
1. `whoami`: show effective userid who logged on in current tty
1. `who am i`: print logged userid

假设用户以`abc`账号登录并`su`到`root`账号时，`whoami`将显示`root`，`who am i`将显示`abc`。

### Linux字符界面下如何翻屏？

`Linux`字符界面下一般不直接进行翻屏，而是使用`more`、`less`之类支持翻屏的命令，如果确实需要翻屏使用`<SHIFT> + <PageUp>`向上翻屏，`<SHIFT> + <PageDown>`向下翻屏，**但能翻屏的范围很小**。

## CentOS使用类

### CentOS 7忘记root密码，怎么办？

#### 方法01： emergency mode

![](./assets_image/F01_startup.png)

![](./assets_image/F02_edit.png)

![](./assets_image/F03_edit.png)

![](./assets_image/F04_passwd.png)

参考自： <https://blog.51cto.com/jschu/1706020>

#### 方法02： single user mode

请参考： <https://blog.csdn.net/ywd1992/article/details/83538730>

<!--
### CentOS 7删除了yum程序，怎么办？

依次运行以下命令：

1. `curl -O http://mirror.centos.org/centos/7/os/x86_64/Packages/yum-3.4.3-167.el7.centos.noarch.rpm`
1. `curl -O http://mirror.centos.org/centos/7/os/x86_64/Packages/yum-metadata-parser-1.1.4-10.el7.x86_64.rpm`
1. `curl -O http://mirror.centos.org/centos/7/os/x86_64/Packages/yum-plugin-fastestmirror-1.1.31-53.el7.noarch.rpm`
1. `rpm -ivh yum-3.4.3-167.el7.centos.noarch.rpm yum-metadata-parser-1.1.4-10.el7.x86_64.rpm yum-plugin-fastestmirror-1.1.31-53.el7.noarch.rpm`
-->

## CentOS故障类

### CentOS 7 yum时出现“Could not resolve host: mirrorlist.centos.org: Unknown error”错误

:point_right: **鸣谢**： 此故障分析定位与解决来自于`gitee`用户@Grade_Harry发出的[`issue #I1G6GR`](https://gitee.com/aroming/course_os/issues/I1G6GR)，#I1G6GR

![could_not_resolve_host_mirrorlist](./assets_image/could_not_resolve_host_mirrorlist.jpg)

1. 故障场景
    1. 机器设备： `VirtualBox`虚拟机
    1. `Guest OS`： `CentOS 7 minimal`
    1. 其他描述： 默认安装`CentOS 7 minimal`后没有执行过`yum`、没有安装其他软件、没有进行系统设置更改
    1. 登录账号： `root`

#### 故障分析定位

可能的故障原因：

1. `Guest OS`网络不通
1. `DNS`无法访问
1. `mirrorlist.centos.org`服务器无法访问

经`ping www.baidu.com`失败，定位于`Guest OS`网络不通，进一步运行`ip addr`发现虚拟网卡无`ip address`，经`cat /etc/sysconfig/network-scripts/ifcfg-enp0s3`（不同机器的虚拟网卡名称可能不一样）发现`ONBOOT=no`

:warning: :warning: :warning: 实际情况下，也有可能由于`DNS`无法访问或`mirrorlist.centos.org`服务器无法访问导致此故障，如是由于此两类原因，请另分析与定位。

#### 故障解决 - 网络不通

1. 运行`vi /etc/sysconfig/network-scripts/ifcfg-enp0s3`（不同机器的虚拟网卡名称可能不一样），将`ONBOOT=no`修改为`ONBOOT=yes`，具体步骤如下：
    1. 将光标移至`ONBOOT=no`
    1. 按下`<a>`键或`<i>`键进入编辑模式
    1. 将`ONBOOT=no`修改为`ONBOOT=yes`
    1. 按下`<ESC>`键退出编辑模式
    1. 输入`:wq`退出并保存文件
1. 运行`service network restart`
